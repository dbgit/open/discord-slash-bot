import * as webpack from 'webpack';
import { resolve } from 'path';
import ForkTsCheckerWebpackPlugin from 'fork-ts-checker-webpack-plugin';
// doesn't want to be resolved properly using the es-module syntax
const TerserPlugin = require('terser-webpack-plugin');
const AwsSamPlugin = require('aws-sam-webpack-plugin');

const awsSamPlugin = new AwsSamPlugin({ vscodeDebug: false });
const isProduction = process.env.NODE_ENV === 'production';

const config: webpack.Configuration = {
	mode: isProduction ? 'production' : 'development',
	target: 'node',
	entry: () => awsSamPlugin.entry(),
	output: {
		filename: (chunkData) => awsSamPlugin.filename(chunkData),
		libraryTarget: 'commonjs2',
		path: resolve('.'),
	},
	resolve: {
		extensions: ['.ts', '.js'],
	},
	externals: [/^aws-sdk.*/],
	plugins: [
		new ForkTsCheckerWebpackPlugin(),
		awsSamPlugin,
	],
	optimization: {
		minimize: isProduction,
		minimizer: [new TerserPlugin({
			extractComments: false,
		})],
	},
	module: {
		rules: [
			{
				test: /\.ts?$/,
				loader: 'ts-loader',
				exclude: /node_modules/,
				options: {
					configFile: 'tsconfig-prod.json',
					transpileOnly: true,
					experimentalFileCaching: true,
					onlyCompileBundledFiles: true
				}
			}
		],
	},
};

export default config;
