import { Config } from '@jest/types';

const fileGlob = '**/__tests__/*.test.ts';

const config: Config.InitialOptions = {
	preset: 'ts-jest',
	roots: [
		'<rootDir>/src',
	],
	transform: {
		'^.+\\.ts$': 'ts-jest',
	},
	testMatch: [
		fileGlob,
	],
	collectCoverage: true,
	coverageDirectory: './coverage',
	coverageReporters: [
		'json',
		'text',
	],
	verbose: true,
};

export default config;
