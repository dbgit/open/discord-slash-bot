# Discord Webhook Bot

## Add to Server

`https://discord.com/api/oauth2/authorize?client_id=[[APPLICATION_ID]]&scope=applications.commands`

## Get Command registration token

`https://discord.com/api/oauth2/authorize?client_id=[[APPLICATION_ID]]&redirect_uri=https%3A%2F%2F[[CALLBACK_DOMAIN]]%2Fv1%2Flogin&response_type=code&scope=applications.commands.update`
