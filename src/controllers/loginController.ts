import { Request, Response } from 'lambda-api';
import { URLSearchParams } from 'url';
import { AxiosInstance } from 'axios';

export class LoginController {
	private readonly options: LoginControllerOptions;

	private readonly axiosClient: AxiosInstance;

	constructor(options: LoginControllerOptions, axiosClient: AxiosInstance) {
		this.options = options;
		this.axiosClient = axiosClient;
	}

	public async post(req: Request, res: Response): Promise<void> {
		const { code } = req.query;

		if (!code) {
			res.sendStatus(400);
			return;
		}

		const params = new URLSearchParams();
		params.append('client_id', this.options.clientId);
		params.append('client_secret', this.options.clientSecret);
		params.append('grant_type', 'authorization_code');
		params.append('code', code);
		params.append('redirect_uri', this.options.redirectUri);
		params.append('scope', 'applications.commands.update');

		const { data } = await this.axiosClient.request({
			method: 'post',
			url: 'https://discord.com/api/oauth2/token',
			data: params.toString(),
			headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
			},
		});

		res.status(200);
		res.json(data);
	}
}

interface LoginControllerOptions {
	clientId: string;
	clientSecret: string;
	redirectUri: string;
}
