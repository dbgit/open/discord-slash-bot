import { Request, Response } from 'lambda-api';
import { AxiosInstance } from 'axios';
import { CommandBase } from '../commands/commandBase';
import { ApplicationCommand, ApplicationCommandPartial } from '../lib/interactionModels';

export class RegistrationController {
	private readonly applicationId: string;

	private readonly commands: Record<string, CommandBase>;

	private readonly axiosClient: AxiosInstance;

	constructor(applicationId: string, commands: Record<string, CommandBase>, axiosClient: AxiosInstance) {
		this.applicationId = applicationId;
		this.commands = commands;
		this.axiosClient = axiosClient;
	}

	public async post(req: Request, res: Response): Promise<void> {
		const token = req.headers.authorization;
		if (!token?.startsWith('Bearer ')) {
			res.status(401);
			res.send({ message: 'No Bearer token provided' });
			return;
		}

		const actions = await this.getCommandOperations(token);

		for (const cmd of actions.delete) {
			await this.deleteCommand(token, cmd.id);
		}

		for (const cmd of actions.update) {
			await this.updateCommand(token, cmd.id, cmd);
		}

		for (const cmd of actions.create) {
			await this.registerCommand(token, cmd);
		}

		res.status(200);
		res.json({
			created: actions.create.map((cmd) => cmd.name),
			updated: actions.update.map((cmd) => cmd.name),
			deleted: actions.delete.map((cmd) => cmd.name),
		});
	}

	private async getCommandOperations(token: string): Promise<CommandActionSort> {
		const registeredCommands = await this.getRegisteredCommands(token);

		const actions: CommandActionSort = {
			create: [],
			update: [],
			delete: [],
		};

		const stripInstance = (instance: CommandBase): ApplicationCommandPartial => ({
			name: instance.name,
			description: instance.description,
			options: instance.options,
		});

		for (const [name, instance] of Object.entries(this.commands)) {
			const registered = registeredCommands.find((cmd) => cmd.name === name);
			if (registered) {
				actions.update.push({
					...registered,
					...stripInstance(instance),
				});
			} else {
				actions.create.push(stripInstance(instance));
			}
		}

		const commandNames = Object.keys(this.commands);
		for (const rcmd of registeredCommands) {
			const deleted = commandNames.find((cmd) => cmd !== rcmd.name);
			if (deleted) {
				actions.delete.push(rcmd);
			}
		}

		return actions;
	}

	private async getRegisteredCommands(token: string): Promise<ApplicationCommand[]> {
		const response = await this.axiosClient.request<ApplicationCommand[]>({
			method: 'GET',
			url: this.makeEndpoint(`applications/${this.applicationId}/commands`),
			headers: {
				Authorization: token,
			},
		});

		return response.data;
	}

	private async registerCommand(token: string, command: ApplicationCommandPartial, commandId?: string): Promise<ApplicationCommand> {
		const suffix = commandId ? `/${commandId}` : '';

		const response = await this.axiosClient.request<ApplicationCommand>({
			method: commandId ? 'PATCH' : 'POST',
			url: this.makeEndpoint(`applications/${this.applicationId}/commands${suffix}`),
			headers: {
				Authorization: token,
			},
			data: command,
		});

		return response.data;
	}

	private async updateCommand(token: string, commandId: string, command: ApplicationCommand): Promise<ApplicationCommand> {
		return this.registerCommand(token, command, commandId);
	}

	private async deleteCommand(token: string, commandId: string): Promise<boolean> {
		const request = await this.axiosClient.request({
			method: 'DELETE',
			url: this.makeEndpoint(`applications/${this.applicationId}/commands/${commandId}`),
			headers: {
				Authorization: token,
			},
		});

		return request.status === 204;
	}

	private makeEndpoint(endpoint: string): string {
		return `https://discord.com/api/v8/${endpoint}`;
	}
}

interface CommandActionSort {
	create: ApplicationCommandPartial[];
	update: ApplicationCommand[];
	delete: ApplicationCommand[];
}
