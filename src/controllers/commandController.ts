import { Request, Response } from 'lambda-api';
import { AxiosInstance } from 'axios';
import { Interaction, InteractionType } from '../lib/interactionModels';
import { CommandBase } from '../commands/commandBase';

export class CommandController {
	private readonly commands: Record<string, CommandBase>;

	private axiosClient: AxiosInstance;

	constructor(commands: Record<string, CommandBase>, axiosClient: AxiosInstance) {
		this.commands = commands;
		this.axiosClient = axiosClient;
	}

	public async post(req: Request, res: Response): Promise<void> {
		switch (req.body.type) {
			case InteractionType.Ping:
				res.status(200);
				res.json({
					type: InteractionType.Ping,
				});
				return;
			case InteractionType.ApplicationCommand:
				await this.execCommand(req, res);
				return;
			default:
				this.unknownType(res);
		}
	}

	private async execCommand(req: Request, res: Response) {
		const interaction: Interaction = req.body;
		if (!interaction.data) {
			res.sendStatus(400);
			return;
		}

		const commandExecutor = this.commands[interaction.data.name];
		if (!commandExecutor) {
			res.sendStatus(404);
			return;
		}

		const result = await commandExecutor.execute(interaction);

		await this.axiosClient.request({
			method: 'post',
			url: `https://discord.com/api/v8/interactions/${interaction.id}/${interaction.token}/callback`,
			data: result,
		});

		res.sendStatus(204);
	}

	private unknownType(res: Response): void {
		res.status(501);
		res.json({
			message: 'Interaction type not supported',
		});
	}
}
