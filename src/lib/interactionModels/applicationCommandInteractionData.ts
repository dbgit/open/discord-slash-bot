import { ApplicationCommandInteractionDataOption } from './applicationCommandInteractionDataOption';
import { Snowflake } from './snowflake';

/**
 * @link https://discord.com/developers/docs/interactions/slash-commands#interaction-applicationcommandinteractiondata
 */
export interface ApplicationCommandInteractionData {
	/**
	 * the ID of the invoked command
	 */
	id: Snowflake;

	/**
	 * the name of the invoked command
	 */
	name: string;

	/**
	 * the params + values from the user
	 */
	options?: ApplicationCommandInteractionDataOption[];
}
