import { Snowflake } from './snowflake';

/**
 * The allowed mention field allows for more granular control over mentions without various hacks to the message content.
 *
 * This will always validate against message content to avoid phantom pings (e.g. to ping everyone, you must still have @everyone in the message content),
 * and check against user/bot permissions.
 *
 * @link https://discord.com/developers/docs/resources/channel#allowed-mentions-object
 */
export interface AllowedMentions {
	/**
	 * An array of allowed mention types to parse from the content.
	 * @link https://discord.com/developers/docs/resources/channel#allowed-mentions-object-allowed-mention-types
	 */
	parse: AllowedMentionsTypes[];

	/**
	 * Array of role_ids to mention (Max size of 100)
	 */
	roles: Snowflake[];

	/**
	 * Array of user_ids to mention (Max size of 100)
	 */
	users: Snowflake[];

	/**
	 * For replies, whether to mention the author of the message being replied to (default false)
	 * @default false
	 */
	replied_user: boolean;
}

export enum AllowedMentionsTypes {
	ROLE = 'roles',
	USER = 'users',
	EVERYONE = 'everyone',
}
