/**
 * @link https://discord.com/developers/docs/interactions/slash-commands#interaction-interactiontype
 */
export enum InteractionType {
	Ping = 1,
	ApplicationCommand = 2,
}
