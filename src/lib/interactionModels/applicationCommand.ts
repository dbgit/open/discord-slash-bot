import { Snowflake } from './snowflake';
import { ApplicationCommandPartial } from './applicationCommandPartial';

/**
 * An application command is the base "command" model that belongs to an application. This is what you are creating when you POST a new command.
 * @link https://discord.com/developers/docs/interactions/slash-commands#applicationcommand
 */
export interface ApplicationCommand extends ApplicationCommandPartial {
	/**
	 * unique id of the command
	 */
	id: Snowflake;

	/**
	 * unique id of the parent application
	 */
	application_id: Snowflake;
}
