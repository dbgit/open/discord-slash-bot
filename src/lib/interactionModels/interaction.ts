import { Snowflake } from './snowflake';
import { InteractionType } from './interactionType';
import { ApplicationCommandInteractionData } from './applicationCommandInteractionData';
import { GuildMember } from './guildMember';

/**
 * An interaction is the base "thing" that is sent when a user invokes a command, and is the same for Slash Commands and other future interaction types
 * @link https://discord.com/developers/docs/interactions/slash-commands#interaction
 */
export interface Interaction {
	/**
	 * id of the interaction
	 */
	id: Snowflake;

	/**
	 * the type of interaction
	 */
	type: InteractionType;

	/**
	 * the command data payload
	 *
	 * This is always present on ApplicationCommandPartial interaction types. It is optional for future-proofing against new interaction types
	 */
	data?: ApplicationCommandInteractionData;

	/**
	 * the guild it was sent from
	 */
	guild_id: Snowflake;

	/**
	 * the channel it was sent from
	 */
	channel_id: Snowflake;

	/**
	 * guild member data for the invoking user
	 */
	member: GuildMember;

	/**
	 * a continuation token for responding to the interaction
	 */
	token: string;

	/**
	 * read-only property, always 1
	 */
	version: number;
}
