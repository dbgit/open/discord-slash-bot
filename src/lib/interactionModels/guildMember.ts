import { Snowflake } from './snowflake';
import { User } from './user';

/**
 * @link https://discord.com/developers/docs/resources/guild#guild-member-object
 */
export interface GuildMember {
	/**
	 * the user this guild member represents
	 */
	user: User;

	/**
	 * this users guild nickname
	 */
	nick?: string;

	/**
	 * array of role object ids
	 * @link https://discord.com/developers/docs/topics/permissions#role-object
	 */
	roles: Snowflake[];

	/**
	 * when the user joined the guild
	 */
	joined_at: Date;

	/**
	 * when the user started boosting the guild
	 * @link https://support.discord.com/hc/en-us/articles/360028038352-Server-Boosting-
	 */
	premium_since?: Date;

	/**
	 * whether the user is deafened in voice channels
	 */
	deaf?: boolean;

	/**
	 * whether the user is muted in voice channels
	 */
	mute?: boolean;

	/**
	 * whether the user has passed the guild's Membership Screening requirements
	 */
	is_pending: false;
}
