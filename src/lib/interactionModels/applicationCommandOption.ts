import { ApplicationCommandOptionType } from './applicationCommandOptionType';
import { ApplicationCommandOptionChoice } from './applicationCommandOptionChoice';

/**
 * You can specify a maximum of 10 choices per option
 *
 * @link https://discord.com/developers/docs/interactions/slash-commands#applicationcommandoption
 */
export interface ApplicationCommandOption {
	/**
	 * value of ApplicationCommandOptionType
	 *
	 * @link https://discord.com/developers/docs/interactions/slash-commands#applicationcommandoptiontype
	 */
	type: ApplicationCommandOptionType;

	/**
	 * 1-32 character name
	 */
	name: string;

	/**
	 * 1-100 character description
	 */
	description: string;

	/**
	 * the first required option for the user to complete--only one option can be default
	 */
	default?: boolean;

	/**
	 * if the parameter is required or optional--default false
	 */
	required?: boolean;

	/**
	 * choices for string and int types for the user to pick from
	 */
	choices?: ApplicationCommandOptionChoice[];

	/**
	 * if the option is a subcommand or subcommand group type, this nested options will be the parameters
	 */
	options?: ApplicationCommandOption[]
}
