/**
 * @link https://discord.com/developers/docs/interactions/slash-commands#applicationcommandoptionchoice
 */
export interface ApplicationCommandOptionChoice {
	/**
	 * 1-100 character choice name
	 */
	name: string;

	/**
	 * value of the choice
	 */
	value: string | number;
}
