import { AllowedMentions } from './allowedMentions';
import { Embed } from './embed';

/**
 * Not all message fields are currently supported.
 * @link https://discord.com/developers/docs/interactions/slash-commands#interaction-interactionapplicationcommandcallbackdata
 */
export interface InteractionApplicationCommandCallbackData {
	/**
	 * is the response TTS
	 * @default false
	 */
	tts?: boolean;

	/**
	 * message content
	 */
	content: string;

	/**
	 * supports up to 10 embeds
	 */
	embeds?: Embed[];

	/**
	 * allowed mentions object
	 */
	allowed_mentions?: AllowedMentions;
}
