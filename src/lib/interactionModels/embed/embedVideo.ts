/**
 * @link https://discord.com/developers/docs/resources/channel#embed-object-embed-video-structure
 */
export interface EmbedVideo {
	/**
	 * source url of video
	 */
	url?: string;

	/**
	 * height of video
	 */
	height?: number;

	/**
	 * width of video
	 */
	width?: number;
}
