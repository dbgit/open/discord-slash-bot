import { EmbedType } from './embedType';
import { EmbedFooter } from './embedFooter';
import { EmbedImage } from './embedImage';
import { EmbedThumbnail } from './embedThumbnail';
import { EmbedVideo } from './embedVideo';
import { EmbedProvider } from './embedProvider';
import { EmbedAuthor } from './embedAuthor';
import { EmbedField } from './embedField';

/**
 * @link https://discord.com/developers/docs/resources/channel#embed-object
 */
export interface Embed {
	/**
	 * 	title of embed
	 */
	title?: string;

	/**
	 * 	type of embed (always "rich" for webhook embeds)
	 * 	@default rich
	 * 	@deprecated
	 */
	type?: EmbedType;

	/**
	 * description of embed
	 */
	description?: string;

	/**
	 * url of embed
	 */
	url?: string;

	/**
	 * timestamp of embed content (ISO8601)
	 */
	timestamp?: Date;

	/**
	 * color code of the embed
	 */
	color?: number;

	/**
	 * footer information
	 */
	footer?: EmbedFooter;

	/**
	 * image information
	 */
	image?: EmbedImage;

	/**
	 * thumbnail information
	 */
	thumbnail?: EmbedThumbnail;

	/**
	 * video information
	 */
	video?: EmbedVideo;

	/**
	 * provider information
	 */
	provider?: EmbedProvider;

	/**
	 * author information
	 */
	author?: EmbedAuthor;

	/**
	 * fields information
	 */
	fields: EmbedField[];
}
