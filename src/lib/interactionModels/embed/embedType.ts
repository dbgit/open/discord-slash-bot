/**
 * Embed types are "loosely defined" and, for the most part, are not used by our clients for rendering.
 * Embed attributes power what is rendered.
 * Embed types should be considered deprecated and might be removed in a future API version.
 *
 * @deprecated
 * @link https://discord.com/developers/docs/resources/channel#embed-object-embed-types
 */
export enum EmbedType {
	/**
	 * generic embed rendered from embed attributes
	 */
	Rich = 'rich',

	/**
	 * image embed
	 */
	Image = 'image',

	/**
	 * video embed
	 */
	Video = 'video',

	/**
	 * animated gif image embed rendered as a video embed
	 */
	Gifv = 'gifv',

	/**
	 * article embed
	 */
	Article = 'article',

	/**
	 * link embed
	 */
	Link = 'link',
}
