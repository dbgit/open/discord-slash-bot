/**
 * All options have names, and an option can either be a parameter and input value--in which case value will be set--or it can denote
 * a subcommand or group--in which case it will contain a top-level key and another array of options.
 *
 * `value` and `options` are mutually exclusive.
 *
 * @link https://discord.com/developers/docs/interactions/slash-commands#interaction-applicationcommandinteractiondataoption
 */
export interface ApplicationCommandInteractionDataOption {
	/**
	 * the name of the parameter
	 */
	name: string;

	/**
	 * the value of the pair
	 */
	value?: string;

	/**
	 * present if this option is a group or subcommand
	 */
	options?: ApplicationCommandInteractionDataOption[];
}
