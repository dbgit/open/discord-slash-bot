/**
 * @link https://discord.com/developers/docs/interactions/slash-commands#applicationcommandoptiontype
 */
export enum ApplicationCommandOptionType {
	SubCommand = 1,
	SubCommandGroup =	2,
	String = 3,
	Integer = 4,
	Boolean = 5,
	User = 6,
	Channel = 7,
	Role = 8,
}
