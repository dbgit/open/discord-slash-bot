/**
 * @link https://discord.com/developers/docs/interactions/slash-commands#interaction-interactionresponsetype
 */
export enum InteractionResponseType {
	/**
	 * ACK a Ping
	 */
	Pong = 1,

	/**
	 * ACK a command without sending a message, eating the user's input
	 */
	Acknowledge = 2,

	/**
	 * respond with a message, eating the user's input
	 */
	ChannelMessage = 3,

	/**
	 * respond with a message, showing the user's input
	 */
	ChannelMessageWithSource = 4,

	/**
	 * ACK a command without sending a message, showing the user's input
	 */
	ACKWithSource = 5,
}
