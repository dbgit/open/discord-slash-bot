import { ApplicationCommandOption } from './applicationCommandOption';

/**
 * Create a new global command. New global commands will be available in all guilds after 1 hour.
 * Returns 200 and an ApplicationCommandPartial object.
 *
 * @link https://discord.com/developers/docs/interactions/slash-commands#create-global-application-command
 */
export interface ApplicationCommandPartial {
	/**
	 * 3-32 character command name
	 */
	name: string;

	/**
	 * 1-100 character description
	 */
	description: string;

	/**
	 * the parameters for the command
	 */
	options?: ApplicationCommandOption[];
}
