import { InteractionResponseType } from './interactionResponseType';
import { InteractionApplicationCommandCallbackData } from './interactionApplicationCommandCallbackData';

/**
 * After receiving an interaction, you must respond to acknowledge it.
 * This may be a pong for a ping, a message, or simply an acknowledgement that you have received it and will handle the command async.
 *
 * Interaction responses may choose to "eat" the user's command input if you do not wish to have their slash command show up as message in chat.
 * This may be helpful for slash commands, or commands whose responses are asynchronous or ephemeral messages.
 *
 * @Link https://discord.com/developers/docs/interactions/slash-commands#interaction
 */
export interface InteractionResponse {
	/**
	 * the type of response
	 */
	type: InteractionResponseType;

	/**
	 * an optional response message
	 */
	data?: InteractionApplicationCommandCallbackData;
}
