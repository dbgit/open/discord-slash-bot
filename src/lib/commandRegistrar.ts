import * as commands from '../commands';
import { CommandBase } from '../commands/commandBase';

export const loadCommands = (): Record<string, CommandBase> => {
	const cmds: Record<string, CommandBase> = {};
	for (const command of Object.entries(commands)) {
		const cmd = new command[1]();
		cmds[cmd.name] = cmd;
	}
	return cmds;
};
