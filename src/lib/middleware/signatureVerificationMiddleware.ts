import { sign } from 'tweetnacl';
import { Request, Response } from 'lambda-api';

export class SignatureVerificationMiddleware {
	private readonly publicKey: string;

	constructor(publicKey: string) {
		this.publicKey = publicKey;
	}

	public async verifySignature(req: Request, res: Response, next: () => void): Promise<void> {
		const signature = req.rawHeaders?.['X-Signature-Ed25519'];
		const timestamp = req.rawHeaders?.['X-Signature-Timestamp'];
		if (signature && timestamp) {
			if (await sign.detached.verify(Buffer.from(timestamp + req.rawBody), Buffer.from(signature, 'hex'), Buffer.from(this.publicKey, 'hex'))) {
				next();
				return;
			}
		}

		res.status(401);
		res.json({ message: 'go away' });
	}
}
