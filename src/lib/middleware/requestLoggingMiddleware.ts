import { Request, Response } from 'lambda-api';

export const requestLoggingMiddleware = (req: Request, res: Response, next: () => void): void => {
	// eslint-disable-next-line no-console
	console.log(JSON.stringify({
		path: req.path,
		query: req.query,
		method: req.method,
		headers: req.rawHeaders,
		body: req.body,
	}));

	next();
};
