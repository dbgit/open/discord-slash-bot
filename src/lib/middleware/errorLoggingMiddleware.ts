import { Request, Response } from 'lambda-api';

const errorToObject = (error: Error): Record<string, string> => {
	const obj: Record<string, string> = {};
	obj.name = error.name;
	Object.getOwnPropertyNames(error).forEach((res) => {
		// @ts-ignore
		obj[res] = error[res];
	});
	return obj;
};

export const errorLoggingMiddleware = (error: Error, req: Request, res: Response, next: () => void): void => {
	const obj = errorToObject(error);
	// eslint-disable-next-line no-console
	console.error(JSON.stringify(obj));

	next();
};
