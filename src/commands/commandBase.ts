import { ApplicationCommandOption, Interaction, InteractionResponse } from '../lib/interactionModels';

export interface CommandBase {
	name: string;
	description: string;
	options: ApplicationCommandOption[];
	execute: (context: Interaction) => (InteractionResponse | Promise<InteractionResponse>);
}
