import { CommandBase } from './commandBase';

import {
	ApplicationCommandOption, ApplicationCommandOptionType, Interaction, InteractionResponse, InteractionResponseType,
} from '../lib/interactionModels';

export class HelloWorldCommand implements CommandBase {
	public readonly name = 'hello';

	public readonly description = 'Running this will let the bot greet you';

	public readonly options: ApplicationCommandOption[] = [
		{
			name: 'name',
			description: 'Say hello to whom?',
			type: ApplicationCommandOptionType.String,
		},
	];

	execute(context: Interaction): InteractionResponse {
		const name = context.data?.options?.find((opt) => opt.name === 'name')?.value?.trim()
			?? context.member.nick
			?? context.member.user.username;

		return {
			type: InteractionResponseType.ChannelMessage,
			data: {
				content: `Hello ${name}!`,
			},
		};
	}
}
