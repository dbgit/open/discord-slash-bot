import lambdaApi from 'lambda-api';
import axios from 'axios';
import { APIGatewayEvent, APIGatewayProxyResult, Context } from 'aws-lambda';
import { RegistrationController } from './controllers/registrationController';
import { loadCommands } from './lib/commandRegistrar';
import { LoginController } from './controllers/loginController';
import { requestLoggingMiddleware } from './lib/middleware/requestLoggingMiddleware';
import { SignatureVerificationMiddleware } from './lib/middleware/signatureVerificationMiddleware';
import { CommandController } from './controllers/commandController';
import { errorLoggingMiddleware } from './lib/middleware/errorLoggingMiddleware';

const config = {
	/* eslint-disable @typescript-eslint/no-non-null-assertion */
	applicationId: process.env.APPLICATION_ID!,
	publicKey: process.env.PUBLIC_KEY!,
	clientSecret: process.env.CLIENT_SECRET!,
	loginRedirectUri: process.env.LOGIN_REDIRECT_URI!,
	/* eslint-enable @typescript-eslint/no-non-null-assertion */
};

/**
 * Helper Classes
 */
const api = lambdaApi();
const axiosClient = axios.create();
const signatureVerificationMiddleware = new SignatureVerificationMiddleware(config.publicKey);

/**
 * Commands
 */
const commands = loadCommands();

/**
 * Controllers
 */
const registrationController = new RegistrationController(config.applicationId, commands, axiosClient);
const loginController = new LoginController({
	clientId: config.applicationId,
	redirectUri: config.loginRedirectUri,
	clientSecret: config.clientSecret,
}, axiosClient);
const commandController = new CommandController(commands, axiosClient);

/**
 * Middleware
 */
api.use((req, res, next) => requestLoggingMiddleware(req, res, next));
// @ts-ignore
api.use((error, req, res, next) => errorLoggingMiddleware(error, req, res, next));
api.use('/v1/commands', async (req, res, next) => signatureVerificationMiddleware.verifySignature(req, res, next));

/**
 * Endpoints
 */
api.get('v1/login', async (req, res) => loginController.post(req, res));
api.post('/v1/register', (req, res) => registrationController.post(req, res));
api.post('/v1/commands', (req, res) => commandController.post(req, res));

/**
 * Handler
 */
export const handler = (event: APIGatewayEvent, context: Context): Promise<APIGatewayProxyResult> => api.run(event, context);
